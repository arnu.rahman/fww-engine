# FWW Engine

## Deskripsi

FWW Airline Engine merupakan solusi/sistem yang ditawarkan kepada salah satu maskapai penerbangan terkemuka di Indonesia yaitu FWW (Flying Without Wing) Airline untuk menggantikan sistem lama mereka AS400 yang sudah ketinggalan zaman. Sistem terbaru ini menawarkan konsep microservice sebagai arsitektur utama dalam membangun sistemnya sehingga dapat menawarkan sistem yang memiliki kriteria scability, secure, high performance and efficient.

FWW Engine memiliki 4 service utama yaitu core, workflow, notifications dan booking. Selain itu sistem ini terintegrasi dengan 2 sistem luar seperti regulation (imigrasi, peduliLindungi dan disdukcapil) dan member (third party). Selain itu FWW Engine menggunakan Payment Gateway dari Midtrans untuk proses charge trasaction dan handle pembayaran.

## Desain

### Arsitektur

![FWW Engine Architecture](https://gitlab.com/arnu.rahman/fww-engine/-/raw/main/docs/architecture/FWW_Architecture.jpg)

### ERDiagram

Berikut adalah rancangan DB yang ada di Service Core Engine.
```mermaid
erDiagram
airport {
  varchar id PK
  varchar icao UK
  varchar iaca UK
  varchar name
  enum category
  enum class
  varchar manage_by
  varchar address
  varchar city
  varchar timezone
  datetime created_at
  datetime updated_at
  datetime deleted_at
}

airplane {
  varchar id PK
  varchar code UK 
  varchar registration_number UK
  varchar name
  int max_business
  int max_economy
  int total_seat
  datetime created_at
  datetime updated_at
  datetime deleted_at
}

airplane_seat {
  varchar id PK
  varchar airplane_id FK
  varchar code
  enum seat_class
  enum seat_side
  enum seat_position
  int seq_row
  datetime created_at
  datetime updated_at
  datetime deleted_at
}

flight {
  varchar id PK
  varchar code
  varchar airplane_id FK
  varchar origin_airport_id FK
  varchar destination_airport_id FK
  time departure_time
  time arrival_time
  time boarding_time
  int duration
  datetime created_at
  datetime updated_at
  datetime deleted_at
}

baggage {
  varchar id PK
  varchar flight_id FK
  smallint capacity
  varchar category
  int charge_price
  datetime created_at
  datetime updated_at
  datetime deleted_at
}

price {
  varchar id PK
  varchar flight_id FK
  enum seat_class
  double price
  datetime created_at
  datetime updated_at
  datetime deleted_at
}

passenger {
  varchar id PK
  enum identity_category
  varchar identity_number UK
  varchar name
  date birth_date
  varchar email
  varchar phone
  datetime created_at
  datetime updated_at
  datetime deleted_at
}

reservation {
  varchar id PK
  varchar booking_code
  varchar reservation_code
  varchar partner_id
  varchar member_id
  varchar passenger_id FK
  varchar flight_id
  date flight_date
  varchar seat_id FK
  double price_actual
  varchar current_status
  datetime created_at
  datetime updated_at
  datetime deleted_at
}

reservation_journey {
  varchar id PK
  varchar reservation_id FK
  varchar description
  datetime journey_time
  datetime created_at
  datetime updated_at
  datetime deleted_at
}

payment {
  varchar id PK
  varchar reservation_id FK
  enum bank_choice
  enum method
  double payment_final
  varchar payment_status
  datetime charge_time
  datetime last_payment_time
  datetime payment_time
  datetime created_at
  datetime updated_at
  datetime deleted_at
}

payment_detail {
  varchar id PK
  varchar payment_id FK
  varchar name
  int quantity
  double price
  datetime created_at
  datetime updated_at
  datetime deleted_at
}

flight }o--|| airplane : contains
baggage }|--|| flight : contains
price }|--|| flight : contains
airplane ||--|{ airplane_seat : contains
flight }o--|| airport : originFlight
flight }o--|| airport : desinationFlight
passenger ||--o{ reservation : makes
reservation }|--|| airplane_seat : chooseSeat
reservation }|--|| flight : chooseFlight
reservation_journey }o--|| reservation : contains
payment }|--|| reservation : contains
payment ||--|{ payment_detail : has
```

<br>Berikut adalah rancangan DB yang ada di Service Booking.

```mermaid
erDiagram
partner {
  varchar id PK
  varchar cognito_id
  varchar name
  varchar username
  varchar email
  varchar phone
  datetime created_at
  datetime updated_at
  datetime deleted_at
}

seat_status {
  varchar id PK
  varchar passenger_identity
  date flight_date
  varchar flight_id
  varchar origin_airport_id
  varchar destination_airport_id
  varchar airplane_id
  varchar seat_id
  enum seat_class
  varchar seat_number
  double price
  varchar status
  datetime created_at
  datetime updated_at
  datetime deleted_at
}

workflow {
  varchar process_instance_id PK
  varchar reservation_id
  datetime created_at
  datetime updated_at
  datetime deleted_at
}

workflow_detail {
  varchar task_id PK
  varchar workflow FK
  varchar activity_name
  datetime created_at
  datetime updated_at
  datetime deleted_at
}

workflow ||--o{ workflow_detail : has
```

### Sequence Diagram

Berikut adalah sequence diagram untuk proses booking.

```mermaid
sequenceDiagram
    actor Passenger
    Passenger->>Partner: Check Flight Schedule
    Partner->>Booking-Engine: Get Flight Candidate
    Booking-Engine->>Core-Engine: Get Flight Candidate
    Core-Engine-->>Booking-Engine: Return List of Flight Candidate
    Booking-Engine-->>Partner: Return List of Flight
    Partner-->>Passenger: List of Flight
    Passenger->>Partner: Check Available Seat
    Partner->>Booking-Engine: Get Available Seat
    Booking-Engine->>Core-Engine: Get Available Seat
    Core-Engine-->>Booking-Engine: Return List of Flight Seat
    Booking-Engine-->>Partner: Return List Available Seat
    Partner-->>Passenger: List Available Seat
    Passenger->>Partner: Make Reservation
    Partner->>Booking-Engine: Send Reservation Value
    Booking-Engine->>Workflow-Engine: Start Workflow
    Workflow-Engine->>Regulation-Engine: Check Regulation
    Regulation-Engine-->>Workflow-Engine: Result Regulation
    Workflow-Engine->>Notification-Engine: Send Status Booking
    Notification-Engine-->>Passenger: Send Booking Code
    Passenger->>Passenger: Make Payment
    Passenger->>Partner: Confirm Payment
    Partner->>Booking-Engine: Confirm Payment
    Booking-Engine->>Workflow-Engine: Get Status Payment
    Workflow-Engine->>Notification-Engine: Send Status Payment
    Workflow-Engine-->>Core-Engine: Update Status Reservation
    Notification-Engine-->>Passenger: Send Reservation Code
```

## Instalasi dengan Docker Compose

```bash
# lakukan `git clone` pada metarepo fww-solution
$ git clone --recursive https://gitlab.com/arnu.rahman/fww-engine.git
```

```bash
# build image and run container
$ docker-compose -f docker-compose.yml --env-file=docker-compose.env up -d
```

```bash
# load data seeder ke database mariadb
$ docker exec -i mariadb mariadb -uroot -ptest12345 fww_core_system < ./docs/migrations/fww_core_system.sql
$ docker exec -i mariadb mariadb -uroot -ptest12345 fww_booking_system < ./docs/migrations/fww_booking_system.sql
$ docker exec -i mariadb mariadb -uroot -ptest12345 fww_member_system < ./docs/migrations/fww_member_system.sql
$ docker exec -i mariadb mariadb -uroot -ptest12345 fww_regulation_system < ./docs/migrations/fww_regulation_system.sql
```

```bash
# Deploy bpmn file ke Camunda Server
1. Download Modeler
2. Open file .bpmn dari ./docs/bpmn/FWW_Reservation_v2.bpmn
3. Deploy BPMN ke Camunda Server (Klik icon deploy di pojok kiri bawah)
4. Cek hasil deploy di Camunda Cookpit
```

\*Link Download Camunda Modeler: [Camunda Modeler](https://camunda.com/download/modeler/)
<br>*Link Camunda Cookpit Dashboard: [Camunda Cookpit](http://localhost:8080/camunda/app/cockpit/default/#/dashboard)

## Langkah-langkah Test API

1. Import file collection dan enviroment json ke postman:
   - ./docs/collection/FWW Booking API Documentation.postman_collection.json
   - ./docs/collection/fww-booking.postman_environment.json
2. Hit API Login pada directory Partner/Login (Asumsi API berikut berada pada third party yang menggunakan FWW Engine)
3. Search Flight Candidate dengan cara hit API Inquiry/Flight Candidate
4. Check available seat dengan cara hit API Inquiry/Available Seat
5. Choose Seat dengan cara hit API Inquiry/Choose Seat
6. Setelah semua beres hit API Reservation/Make Reservation untuk memesan tiket
   - jika berhasil Anda akan menerima email terkait status reservation (email harus yang sudah terdaftar pada Amazon SES)
   - email dapat berupa decline dan normal email tergantung validasi dari camunda untuk regulation tiket
7. Kemudian hit API Payment/Commit Payment untuk merubah status seat
8. Hit API Payment/Charge Payment untuk mentrigger tagihan ke Midtrans
9. Open link berikut untuk [Midtrans Payment Simulator](https://simulator.sandbox.midtrans.com/openapi/va/index?bank=mandiri)
   untuk merekayasa pembayaran (karena ini masih menggunakan sandbox)
10. Hit API Payment/Confirm Payment untuk mengecek status pembayaran
11. Proses terakhir akan ada email yang menginfokan pembayaran berhasil atau tidak

    \*(jangan lupa input header bearer token dari partner/login untuk semua API dari service-booking)
