-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: fww_booking_system
-- ------------------------------------------------------
-- Server version	11.1.2-MariaDB-1:11.1.2+maria~ubu2204

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `partner`
--

DROP TABLE IF EXISTS `partner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `partner` (
  `id` varchar(36) NOT NULL,
  `cognito_id` varchar(36) NOT NULL,
  `name` varchar(20) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `created_at` datetime(6) NOT NULL DEFAULT current_timestamp(6),
  `updated_at` datetime(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6),
  `deleted_at` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_2c36ac7258d87e16d7b4b425f0` (`cognito_id`),
  UNIQUE KEY `IDX_58a21b0c10b187973cfb0f67ee` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partner`
--

LOCK TABLES `partner` WRITE;
/*!40000 ALTER TABLE `partner` DISABLE KEYS */;
INSERT INTO `partner` VALUES ('1aa32afa-4561-4d35-877f-455e40184d3e','ecc78f1b-d698-444c-81a3-0f98a40d6037','PT Pergi Pergi Asik','pergipergi','arnu12rahman@gmail.com','+6287808210628','2023-11-24 02:48:42.123331','2023-11-24 02:48:42.123331',NULL);
/*!40000 ALTER TABLE `partner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seat_status`
--

DROP TABLE IF EXISTS `seat_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `seat_status` (
  `id` varchar(36) NOT NULL,
  `passenger_identity` varchar(16) NOT NULL,
  `flight_date` date NOT NULL,
  `flight_id` varchar(36) NOT NULL,
  `origin_airport_id` varchar(36) NOT NULL,
  `destination_airport_id` varchar(36) NOT NULL,
  `airplane_id` varchar(36) NOT NULL,
  `seat_id` varchar(36) NOT NULL,
  `seat_class` enum('Business','Economy') NOT NULL,
  `seat_number` varchar(10) NOT NULL,
  `price` double NOT NULL,
  `status` varchar(10) NOT NULL,
  `created_at` datetime(6) NOT NULL DEFAULT current_timestamp(6),
  `updated_at` datetime(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6),
  `deleted_at` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_783ecc6b047e94a984704bd130` (`flight_date`,`flight_id`,`seat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seat_status`
--

LOCK TABLES `seat_status` WRITE;
/*!40000 ALTER TABLE `seat_status` DISABLE KEYS */;
INSERT INTO `seat_status` VALUES ('46575a65-153b-4bf0-a189-e5f86d2c3eb5','3674051212900010','2023-11-29','843b1cb6-60d4-4537-9f77-54dc26785047','1f2f4fa4-7eeb-47eb-abcb-178e5170ea30','33757e05-7813-45fd-9861-5763a6859afb','d704c0d1-a093-4d33-91f7-6f54b014e6d6','011d62d6-f9d1-40a5-96bf-e41e8c902352','Economy','28B',598000,'PAID','2023-11-25 23:47:38.498605','2023-11-26 04:50:31.000000',NULL);
/*!40000 ALTER TABLE `seat_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workflow`
--

DROP TABLE IF EXISTS `workflow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `workflow` (
  `process_instance_id` varchar(36) NOT NULL,
  `reservation_id` varchar(36) NOT NULL,
  `created_at` datetime(6) NOT NULL DEFAULT current_timestamp(6),
  `updated_at` datetime(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6),
  `deleted_at` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`process_instance_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workflow`
--

LOCK TABLES `workflow` WRITE;
/*!40000 ALTER TABLE `workflow` DISABLE KEYS */;
INSERT INTO `workflow` VALUES ('14bdbeed-8c17-11ee-a9e5-0242ac110002','59e014f1-0a6c-4273-95d9-6974a51df508','2023-11-26 04:48:45.711548','2023-11-26 04:48:45.711548',NULL);
/*!40000 ALTER TABLE `workflow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workflow_detail`
--

DROP TABLE IF EXISTS `workflow_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `workflow_detail` (
  `workflow` varchar(36) NOT NULL,
  `task_id` varchar(36) NOT NULL,
  `activity_name` varchar(100) NOT NULL,
  `created_at` datetime(6) NOT NULL DEFAULT current_timestamp(6),
  `updated_at` datetime(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6),
  `deleted_at` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`task_id`),
  KEY `FK_4e2644899a5cbba8d2235a07ac3` (`workflow`),
  CONSTRAINT `FK_4e2644899a5cbba8d2235a07ac3` FOREIGN KEY (`workflow`) REFERENCES `workflow` (`process_instance_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workflow_detail`
--

LOCK TABLES `workflow_detail` WRITE;
/*!40000 ALTER TABLE `workflow_detail` DISABLE KEYS */;
INSERT INTO `workflow_detail` VALUES ('14bdbeed-8c17-11ee-a9e5-0242ac110002','14c909c2-8c17-11ee-a9e5-0242ac110002','CheckBlacklistMember','2023-11-26 04:48:46.129609','2023-11-26 04:48:46.129609',NULL),('14bdbeed-8c17-11ee-a9e5-0242ac110002','15133229-8c17-11ee-a9e5-0242ac110002','CheckDisdukcapil','2023-11-26 04:48:46.595904','2023-11-26 04:48:46.595904',NULL),('14bdbeed-8c17-11ee-a9e5-0242ac110002','155e44f0-8c17-11ee-a9e5-0242ac110002','CheckImigration','2023-11-26 04:48:46.775408','2023-11-26 04:48:46.775408',NULL),('14bdbeed-8c17-11ee-a9e5-0242ac110002','1578d1d7-8c17-11ee-a9e5-0242ac110002','CheckPeduliLindungi','2023-11-26 04:48:47.166628','2023-11-26 04:48:47.166628',NULL),('14bdbeed-8c17-11ee-a9e5-0242ac110002','15b62905-8c17-11ee-a9e5-0242ac110002','SendMailBookingCode','2023-11-26 04:48:47.538411','2023-11-26 04:48:47.538411',NULL),('14bdbeed-8c17-11ee-a9e5-0242ac110002','15ebdf0e-8c17-11ee-a9e5-0242ac110002','CommitChargePayment','2023-11-26 04:49:03.229702','2023-11-26 04:49:03.229702',NULL),('14bdbeed-8c17-11ee-a9e5-0242ac110002','1f514456-8c17-11ee-a9e5-0242ac110002','SubmitChargeTransaction','2023-11-26 04:49:26.609493','2023-11-26 04:49:26.609493',NULL),('14bdbeed-8c17-11ee-a9e5-0242ac110002','2d413acf-8c17-11ee-a9e5-0242ac110002','ConfirmPayment','2023-11-26 04:50:31.067041','2023-11-26 04:50:31.067041',NULL),('14bdbeed-8c17-11ee-a9e5-0242ac110002','53a89ecf-8c17-11ee-a9e5-0242ac110002','CheckPayment','2023-11-26 04:50:31.565305','2023-11-26 04:50:31.565305',NULL),('14bdbeed-8c17-11ee-a9e5-0242ac110002','53f02f2b-8c17-11ee-a9e5-0242ac110002','SendMailReservationCode','2023-11-26 04:50:31.979997','2023-11-26 04:50:31.979997',NULL);
/*!40000 ALTER TABLE `workflow_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'fww_booking_system'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-11-26 12:06:48
