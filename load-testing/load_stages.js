import http from "k6/http";
import { check } from "k6";
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";
import { textSummary } from "https://jslib.k6.io/k6-summary/0.0.1/index.js";

//scenario 2 (load test based on stages)
export const options = {
  stages: [
    { duration: "0.3m", target: 500 }, // simulate ramp-up of traffic from 1 to 500 users over 0.3 minute
    { duration: "0.4m", target: 500 }, // stay at 500 users for 0.4 minute
    { duration: "0.6m", target: 1000 }, // simulate ramp-up of traffic from 500 to 1000 users over 0.6 minute
    { duration: "1m", target: 2000 }, // simulate ramp-up to 2000 users over 1 minute
    { duration: "1.4m", target: 0 }, // simulate ramp-down to 0 users over 1.4 minute
  ],
  thresholds: {
    http_req_failed: ["rate<0.001"], // the error rate must be lower than 0.1%
    http_req_duration: ["p(90)<1800"], // 90% of requests must complete below 1800ms
    http_req_receiving: ["max<6000"], // max receive request below 6000ms
  },
};

export default function () {
  const params = {
    headers: {
      "Content-Type": "application/json",
      Authorization:
        "Basic Znd3X2ludGVybmFsX3VzZXJuYW1lOmZ3d19pbnRlcm5hbF9wYXNzd29yZA==",
    },
    timeout: "600s",
  };

  const get_flight = http.get(
    "http://localhost:3001/api/v1/inquiry/flight",
    params
  );
  check(get_flight, {
    "verify success response of get flight": (get_flight) =>
      get_flight.status == 200,
  });
}

export function handleSummary(data) {
  return {
    "summary.json": JSON.stringify(data),
    "result.html": htmlReport(data),
    stdout: textSummary(data, { indent: " ", enableColors: true }),
  };
}
