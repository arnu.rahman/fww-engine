import http from "k6/http";
import { check } from "k6";
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";
import { textSummary } from "https://jslib.k6.io/k6-summary/0.0.1/index.js";

//scenario 1 (load test vuser and duration)
export const options = {
  vus: 350, //simulasi berapa user yg akses URL
  duration: "150s", //berapa lama waktu yg dibutuhkan

  thresholds: {
    http_req_failed: ["rate<0.001"], // the error rate must be lower than 0.1%
    http_req_duration: ["p(90)<5000"], // 90% of requests must complete below 2000ms
    http_req_receiving: ["max<17000"], // max receive request below 17000ms
  },
};

export default function () {
  const params = {
    headers: {
      "Content-Type": "application/json",
      Authorization:
        "Basic Znd3X2ludGVybmFsX3VzZXJuYW1lOmZ3d19pbnRlcm5hbF9wYXNzd29yZA==",
    },
    timeout: '600s'
  };

  const get_flight = http.get(
    "http://localhost:3001/api/v1/inquiry/flight",
    params
  );
  check(get_flight, {
    "verify success response of get flight": (get_flight) =>
      get_flight.status == 200,
  });
}

export function handleSummary(data) {
  return {
    "summary.json": JSON.stringify(data),
    "result.html": htmlReport(data),
    stdout: textSummary(data, { indent: " ", enableColors: true }),
  };
}
