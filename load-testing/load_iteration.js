import http from "k6/http";
import { check } from "k6";
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";
import { textSummary } from "https://jslib.k6.io/k6-summary/0.0.1/index.js";

//scenario 3 (iteration, start time and graceful stop)
export const options = {
  scenarios: {
    shared_iter_scenario: {
      executor: "shared-iterations",
      vus: 100,
      iterations: 200,
      startTime: "0s",
      gracefulStop: "25s",
    },
    per_vu_scenario: {
      executor: "per-vu-iterations",
      vus: 100,
      iterations: 10,
      startTime: "10s",
      gracefulStop: "20s",
    },
  },
  thresholds: {
    http_req_failed: ["rate<0.001"], // the error rate must be lower than 0.1%
    http_req_duration: ["p(90)<1500"], // 90% of requests must complete below 600ms
    http_req_receiving: ["max<150"], // slowest request below 150ms
    iteration_duration: ["p(95)<2500"], // 95% of requests must complete below 20000ms
  },
};

export default function () {
  const params = {
    headers: {
      "Content-Type": "application/json",
      Authorization:
        "Basic Znd3X2ludGVybmFsX3VzZXJuYW1lOmZ3d19pbnRlcm5hbF9wYXNzd29yZA==",
    },
    timeout: "600s",
  };

  const get_flight = http.get(
    "http://localhost:3001/api/v1/inquiry/flight",
    params
  );
  check(get_flight, {
    "verify success response of get flight": (get_flight) =>
      get_flight.status == 200,
  });
}

export function handleSummary(data) {
  return {
    "summary.json": JSON.stringify(data),
    "result.html": htmlReport(data),
    stdout: textSummary(data, { indent: " ", enableColors: true }),
  };
}
