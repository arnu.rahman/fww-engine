CREATE DATABASE IF NOT EXISTS `fww_booking_system`;
CREATE DATABASE IF NOT EXISTS `fww_core_system`;
CREATE DATABASE IF NOT EXISTS `fww_member_system`;
CREATE DATABASE IF NOT EXISTS `fww_regulation_system`;